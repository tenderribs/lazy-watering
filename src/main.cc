#include <Arduino.h>

const unsigned humiditySensor = A1;
const unsigned power_led_pin = 9;
const unsigned power_on = 2;
const unsigned relay = 7;

const unsigned airVal = 520;
const unsigned waterVal = 252;

const unsigned dry_interval = (airVal - waterVal) / 3; // upper third of interval is considered dry
unsigned buttonState = 0;

void setup() {
    pinMode(humiditySensor, INPUT);
    pinMode(power_led_pin, OUTPUT);
    pinMode(relay, OUTPUT);

    digitalWrite(power_led_pin, HIGH);
    Serial.begin(115200);
}

bool isDry(const unsigned int& moisture) {
    return moisture >= (airVal - dry_interval);
}

void loop() {
    if (digitalRead(power_on)) {
        unsigned int moistureLevel = analogRead(humiditySensor);

        if (isDry(moistureLevel)) { // turn on pump for 1 second
            digitalWrite(relay, HIGH);
            delay(1000);
            digitalWrite(relay, LOW);
        }
    }

    delay(2000); // chill for 2 seconds
}
