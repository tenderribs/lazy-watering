# Automatic Plant Watering

I usually only have plastic plants in my room, because sometimes I forget to water the real ones. Since I was gifted some potted flowers and accidentally let them dry out a few times, I decided to automate the process.

The setup involves a sprinkling can with a small aquarium pump inside. The control box reads the soil moisture level and commands the pump to spray some water for 1s if the soil is deemed dry. Then it waits 2s and checks again and repeats the process if necessary. The hose is clipped on to the pot and coiled around the plant using some inserts available in `/parts`. It has some holes drilled into it to dissipate the water in the soil.

The box contains some random microcontroller I had and a relay. This is normally opened and only closes the pump circuit if the signal pin is pulled high by the arduino. There is also a buck converter to step down the 12V from the power brick to 6.6 Volts, which is what the microcontroller takes as input.

https://wiki.dfrobot.com/Dreamer_Nano_V4.0__DFR0213_
